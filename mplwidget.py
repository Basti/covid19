from PySide2.QtWidgets import QWidget, QVBoxLayout
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure


class MplWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        # fig = Figure(figsize=(7, 5), dpi=65, facecolor=(1, 1, 1), edgecolor=(0, 0, 0))
        fig = Figure()
        self.canvas = FigureCanvas(fig)
        self.toolbar = NavigationToolbar(self.canvas, self)
        vertical_layout = QVBoxLayout()
        vertical_layout.addWidget(self.toolbar)
        vertical_layout.addWidget(self.canvas)
        self.canvas.axes = self.canvas.figure.add_subplot(111)
        self.setLayout(vertical_layout)

    def set_title(self, title):
        self.canvas.axes.set_title(title)

    def set_xlabel(self, label):
        self.canvas.axes.set_xlabel(label)

    def set_ylabel(self, label):
        self.canvas.axes.set_ylabel(label)

    def clear(self):
        self.canvas.axes.clear()


if __name__ == "__main__":
    import sys
    from PySide2.QtWidgets import QApplication
    app = QApplication(sys.argv)
    matplotlib_widget = MplWidget()
    matplotlib_widget.show()
    sys.exit(app.exec_())
