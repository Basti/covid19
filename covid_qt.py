#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import pkg_resources
from PySide2.QtWidgets import QWidget, QHBoxLayout
from PySide2.QtCore import QFile
from PySide2.QtUiTools import QUiLoader
from mplwidget import MplWidget
from CovidDataHolder import CovidDataHolder
from CovidAnalytics import CovidAnalytics
import threading


class CovidMainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.database = None
        thd = threading.Thread(target=self.load_database)
        thd.start()
        self.control_widget = None
        self.graph_widget = None
        self.dispatch = {'Cases': 'plot_cases', 'Daily Cases': 'plot_daily_cases',
                         'Deaths': 'plot_deaths', 'Prevalence rate': 'plot_prevalence',
                         'Incidence rate': 'plot_incidence', 'Death rate': 'plot_death_rate'}
        self.analytics = None
        self.set_ui()
        thd.join()
        self.fill_ui_lists()
        self.set_analytics()
        self.update_graph()

    def load_database(self):
        self.database = CovidDataHolder.get_instance()

    def set_ui(self):
        self.setWindowTitle('COVID Graphs')
        ui_file_name = pkg_resources.resource_filename(__name__, './covid_widget.ui')
        self.load_control_ui(ui_file_name)
        self.graph_widget = MplWidget()
        layout = QHBoxLayout()
        layout.addWidget(self.control_widget)
        layout.addWidget(self.graph_widget)
        self.setLayout(layout)
        self.control_widget.graph_pushButton.clicked.connect(self.update_graph)
        self.control_widget.country_comboBox.currentTextChanged.connect(self.set_analytics)

    def fill_ui_lists(self):
        # self.control_widget.country_comboBox.addItems(self.database.get_top_countries(nb_countries=30))
        countries = self.database.get_top_countries(nb_countries=30)
        # countries.insert(0, 'world')
        countries = ['World'] + countries
        # print(type(countries))
        self.control_widget.country_comboBox.addItems(countries)
        self.control_widget.graph_comboBox.addItems(sorted(list(self.dispatch.keys())))

    def load_control_ui(self, ui_file_name):
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QFile.ReadOnly):
            print('Cannot open {}: {}'.format(ui_file_name, ui_file.errorString()))
            exit(-1)
        loader = QUiLoader()
        self.control_widget = loader.load(ui_file)
        ui_file.close()

    def set_analytics(self):
        country = self.control_widget.country_comboBox.currentText()
        self.analytics = CovidAnalytics(country)
        self.update_info()

    # def update_interface(self):
    #     # if self.graph_comboBox.currentText() in self.country_graphs:
    #     if 'Country' in self.graph_comboBox.currentText():
    #         self.country_comboBox.setEnabled(True)
    #     else:
    #         self.country_comboBox.setEnabled(False)

    def update_graph(self):
        self.graph_widget.clear()
        selected_graph = self.control_widget.graph_comboBox.currentText()
        f = getattr(self, self.dispatch[selected_graph])
        f()
        self.graph_widget.canvas.draw()

    def update_info(self):
        self.control_widget.results_listWidget.clear()
        date = self.database.get_last_update_date()
        self.control_widget.results_listWidget.addItem('Last update: {} {} {}'.format(date.day, date.month_name(), date.year))
        self.control_widget.results_listWidget.addItem('\nInfo for {}:'.format(self.analytics.country))
        last_data = self.analytics.get_last_infos()
        for k, v in last_data.items():
            self.control_widget.results_listWidget.addItem('- {}: {}'.format(k, v))
        # self.control_widget.results_listWidget.addItem('Last update: {} {} {}'.format(date.day, date.month_name(), date.year))
        # self.control_widget.results_listWidget.addItem('Last update: {} {} {}'.format(date.day, date.month_name(), date.year))

    def __plot(self, data, title, ylabel):
        ax = self.graph_widget.canvas.axes
        data.index = self.analytics.data['days since 1st case'].values
        xlabel = 'Days since 1st confirmed case'
        data.plot(ax=ax, title=title, xlabel=xlabel, ylabel=ylabel)

    def plot_cases(self):
        ser = self.analytics.data['confirmed cases']
        # ser.index = self.analytics.data['days since 1st case'].values
        self.__plot(data=ser, title='Number of confirmed cases in ' + self.analytics.country,
                    ylabel='Confirmed cases')
        # self.__plot(data=self.analytics.data['confirmed cases'],
        #             title='Number of confirmed cases in ' + self.analytics.country,
        #             xlabel='Time', ylabel='Confirmed cases')

    def plot_deaths(self):
        ser = self.analytics.data['deaths']
        # ser.index = self.analytics.data['days since 1st case'].values
        self.__plot(data=ser, title='Number of deaths in ' + self.analytics.country,
                    ylabel='Deaths')

    def plot_daily_cases(self):
        ser = self.analytics.data['daily cases']
        ser.index = self.analytics.data['days since 1st case'].values
        ser.plot.bar(ax=self.graph_widget.canvas.axes, title='Number of daily cases in ' + self.analytics.country,
                 xlabel='Days since 1st confirmed case', ylabel='Confirmed cases per day')

    def plot_prevalence(self):
        ser = self.analytics.data['prevalence']
        # ser.index = self.analytics.data['days since 1st case'].values
        self.__plot(data=ser, title='Prevalence rate in ' + self.analytics.country,
                    ylabel='Prevalence rate (per {})'.format(self.analytics.base_rate))

    def plot_incidence(self):
        ser = self.analytics.data['incidence']
        # ser.index = self.analytics.data['days since 1st case'].values
        self.__plot(data=ser, title='Incidence rate in ' + self.analytics.country,
                    ylabel='Incidence rate (per {} per month)'.format(self.analytics.base_rate))

    def plot_death_rate(self):
        ser = self.analytics.data['death rate']
        # ser.index = self.analytics.data['days since 1st case'].values
        self.__plot(data=ser, title='Death rate in ' + self.analytics.country,
                    ylabel='Death rate (per {} per month)'.format(self.analytics.base_rate))

    # def doubling_times(self):
    #     topcountries = self.get_top_countries(20) # récupérer liste dans GUI
    #     countries = []
    #     inferreddoublingtime = []
    #     recentdoublingtime = []
    #     errors = []
    #     for c in topcountries:
    #         # print(c)
    #         a = self.plot_cases(c, False)
    #         if a:
    #             countries.append(c)
    #             inferreddoublingtime.append(a[0])
    #             errors.append(a[1])
    #             recentdoublingtime.append(a[2])
    #
    #     # d = {'Countries': countries, 'Inferred Doubling Time': inferreddoublingtime, '95%CI': errors,
    #     #      'Recent Doubling Time': recentdoublingtime}
    #
    #     err = pd.DataFrame([errors, [float('NaN') for e in errors]]).T
    #     err.index = countries
    #     err.columns = ['Inferred Doubling Time', 'Recent Doubling Time']
    #
    #     dt = pd.DataFrame({'Inferred Doubling Time': inferreddoublingtime, 'Recent Doubling Time': recentdoublingtime},
    #                       index=countries)
    #     dt = dt[dt['Recent Doubling Time'] < 100]
    #
    #     self.results_listWidget.clear()
    #     self.graph_widget.canvas.axes.clear()
    #     dt.plot.bar(yerr=err, capsize=4, ax=self.graph_widget.canvas.axes)
    #     self.graph_widget.canvas.axes.set_ylabel('Doubling Time (Days)')
    #     self.graph_widget.canvas.axes.set_xlabel('Countries')
    #     self.graph_widget.canvas.axes.set_title('Doubling Time of Cumulative COVID-19 Cases.')
    #     self.graph_widget.canvas.draw()
    #
    # def nb_cases(self):
    #     co = self.__cases.iloc[:, [1, -1]].groupby('Country/Region').sum()
    #     co.columns = ['Cases']
    #     co = co.sort_values(by='Cases', ascending=False)
    #     co['Deaths'] = self.__deaths.iloc[:, [1, -1]].groupby('Country/Region').sum()
    #     co['Recovered'] = self.__recov.iloc[:, [1, -1]].groupby('Country/Region').sum()
    #     n = 20
    #     co = co.iloc[:n, :]
    #     self.results_listWidget.clear()
    #     self.results_listWidget.addItem('Number of COVID-19 cases, death and recovered '
    #                                     'for the 20 countries with the most cases')
    #     self.graph_widget.canvas.axes.clear()
    #     co.plot.bar(ax=self.graph_widget.canvas.axes)
    #     self.graph_widget.canvas.axes.set_title('Number of COVID-19 cases.')
    #     self.graph_widget.canvas.draw()
    #
    # def plot_percent(self, country):
    #     # Get the population of the selected country
    #     pop_country = country
    #     corresp = dict(zip(['US', 'Korea'], ['United States of America', 'Democratic Republic of Korea']))
    #     if pop_country in list(corresp.keys()):
    #         pop_country = corresp[pop_country]
    #     if pop_country not in self.__pop['Country'].values:
    #         print('Country not found')
    #     population = self.__pop.loc[self.__pop['Country'] == pop_country, 'Population'].values[0]
    #     self.results_listWidget.clear()
    #     self.results_listWidget.addItem('Population : ' + str(population))
    #     # Get cases vs time
    #     column = 'Country/Region'
    #     co = self.__cases[self.__cases[column] == country].iloc[:, 4:].T.sum(axis=1)
    #     co = pd.DataFrame(co)
    #     co.columns = ['Cases']
    #     co = co.loc[co['Cases'] > 0]
    #     nb_cases = np.array(co['Cases'])
    #     days = np.arange(nb_cases.size)
    #     nb_cases = nb_cases / population
    #     # Plot
    #     self.graph_widget.canvas.axes.clear()
    #     self.graph_widget.canvas.axes.plot(days, nb_cases, 'ko', label='Original Data')
    #     self.graph_widget.canvas.axes.set_title('COVID-19 Cases in ' + country + '.')
    #     self.graph_widget.canvas.axes.set_xlabel('Days')
    #     self.graph_widget.canvas.axes.set_ylabel('Percentage of population')
    #     # self.graph_widget.canvas.axes.legend(loc='best')
    #     self.graph_widget.canvas.draw()
    #
    # def nb_new_cases(self, country):
    #     column = 'Country/Region'
    #     co = self.__cases[self.__cases[column] == country].iloc[:, 4:].T.sum(axis=1)
    #     co = pd.DataFrame(co)
    #     co.columns = ['Cases']
    #     co = co.loc[co['Cases'] > 0]
    #     nb_cases = np.array(co['Cases'])
    #     new_cases = nb_cases[1:] - nb_cases[:-1]
    #     if min(new_cases) < 0:
    #         new_cases = np.array([n if n > 0 else 0 for n in new_cases])
    #
    #     days = np.arange(new_cases.size)
    #     # Plot
    #     self.graph_widget.canvas.axes.clear()
    #     # self.graph_widget.canvas.axes.plot(days, nb_cases, 'ko', label='Original Data')
    #     self.graph_widget.canvas.axes.bar(days, new_cases)
    #     self.graph_widget.canvas.axes.set_title('COVID-19 New Cases per day in ' + country + '.')
    #     self.graph_widget.canvas.axes.set_xlabel('Days')
    #     self.graph_widget.canvas.axes.set_ylabel('Nb of new cases')
    #     # # self.graph_widget.canvas.axes.legend(loc='best')
    #     self.graph_widget.canvas.draw()


if __name__ == '__main__':
    from PySide2 import QtCore
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
    from PySide2.QtWidgets import QApplication
    app = QApplication()
    frame = CovidMainWindow()
    frame.show()
    exit(app.exec_())
