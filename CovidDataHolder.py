#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import country_converter as coco
import world_bank_data as wb


class CovidDataHolder:
    __instance = None
    __COVID_GLOBAL_CASES_URL = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/' \
                        'csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'
    __COVID_GLOBAL_DEATHS_URL = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/' \
                         'csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_deaths_global.csv'
    __COVID_GLOBAL_RECOVERED_URL = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/' \
                            'csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_recovered_global.csv'
    # __COVIR_FRANCE_DATA = 'https://www.data.gouv.fr/fr/datasets/r/63352e38-d353-4b54-bfd1-f1b3ee1cabd7'
    # case_types = ['confirmed cases', 'deaths', 'recovered']

    def __init__(self):
        if CovidDataHolder.__instance is not None:
            raise Exception("This class is a singleton")
        else:
            CovidDataHolder.__instance = self
        self.__country_col = 'Country/Region'
        self.cases = None
        self.deaths = None
        self.recovered = None
        self.wb_data = pd.DataFrame()
        self.countries = None
        self.__download_data()

    @staticmethod
    def get_instance():
        if CovidDataHolder.__instance is None:
            CovidDataHolder()
        return CovidDataHolder.__instance

    @staticmethod
    def get_wb_data(indicator):
        wb_data = wb.get_series(indicator, mrv=1, simplify_index=True)['Afghanistan'::]
        wb_data['World'] = wb.get_series(indicator, mrv=1, simplify_index=True)['World']
        return wb_data

    def __download_data(self):
        """Download COVID19 data from internet"""
        self.cases = pd.read_csv(CovidDataHolder.__COVID_GLOBAL_CASES_URL)
        self.deaths = pd.read_csv(CovidDataHolder.__COVID_GLOBAL_DEATHS_URL)
        self.recovered = pd.read_csv(CovidDataHolder.__COVID_GLOBAL_RECOVERED_URL)
        self.wb_data['Population'] = self.get_wb_data('SP.POP.TOTL')
        self.wb_data['Population aged above 65'] = self.get_wb_data('SP.POP.65UP.TO')
        # GDP = Gross domestic product = PIB
        self.wb_data['GDP'] = self.get_wb_data('NY.GDP.MKTP.CD')
        # National poverty headcount ratio is the percentage of the population living below the national poverty
        # line(s). National estimates are based on population - weighted subgroup estimates from household surveys.
        self.wb_data['Poverty ratio'] = self.get_wb_data('SI.POV.NAHC')
        # Normalize country names
        self.wb_data.index = coco.convert(names=self.wb_data.index.tolist(), to="name_short", not_found=None)
        self.cases[self.__country_col] = coco.convert(self.cases[self.__country_col], to='name_short', not_found=None)
        self.deaths[self.__country_col] = coco.convert(self.deaths[self.__country_col], to='name_short', not_found=None)
        self.recovered[self.__country_col] = coco.convert(self.recovered[self.__country_col], to='name_short', not_found=None)
        # Update the list of countries
        self.countries = ['World'] + self.cases[self.__country_col].drop_duplicates().tolist()

    def get_top_countries(self, in_terms_of='confirmed cases', nb_countries=10):
        """Get countries with the most cases, deaths, recovered detected"""
        if in_terms_of == 'recovered':
            nb_cases = self.recovered.iloc[:, [1, -1]].groupby(self.__country_col).sum()
        elif in_terms_of == 'deaths':
            nb_cases = self.deaths.iloc[:, [1, -1]].groupby(self.__country_col).sum()
        else:
            nb_cases = self.cases.iloc[:, [1, -1]].groupby(self.__country_col).sum()
        most_recent_date = nb_cases.columns[0]
        nb_cases = nb_cases.sort_values(by=most_recent_date, ascending=False)
        return list(nb_cases.index.values[:nb_countries])

    def get_nb_cases(self, country, in_terms_of='confirmed cases'):
        """Get the number of confirmed cases, deaths or recovered in the selected country"""
        if country.lower() == 'world':
            return self.get_nb_cases_worldwide(in_terms_of)
        elif country not in self.countries:
            raise Exception("This country is not in the database")
        if in_terms_of == 'recovered':
            co = self.recovered[self.recovered[self.__country_col] == country].iloc[:, 4:].T.sum(axis=1)
        elif in_terms_of == 'deaths':
            co = self.deaths[self.deaths[self.__country_col] == country].iloc[:, 4:].T.sum(axis=1)
        else:
            co = self.cases[self.cases[self.__country_col] == country].iloc[:, 4:].T.sum(axis=1)
        co = pd.DataFrame(co)
        co.columns = [in_terms_of]
        co = co.loc[co[in_terms_of] > 0]
        return co[in_terms_of]

    def get_nb_cases_worldwide(self, in_terms_of='confirmed cases'):
        """Get the number of confirmed cases, deaths or recovered in the whole world"""
        if in_terms_of == 'recovered':
            co = self.recovered.iloc[:, 4:].T.sum(axis=1)
        elif in_terms_of == 'deaths':
            co = self.deaths.iloc[:, 4:].T.sum(axis=1)
        else:
            co = self.cases.iloc[:, 4:].T.sum(axis=1)
        co = pd.DataFrame(co)
        co.columns = [in_terms_of]
        co = co.loc[co[in_terms_of] > 0]
        return co[in_terms_of]

    def get_daily_cases(self, country, in_terms_of='confirmed cases'):
        """Get number of new (daily) cases in the selected country"""
        nb_cases = self.get_nb_cases(country, in_terms_of)
        return nb_cases.diff()

    def get_daily_cases_worldwide(self, in_terms_of='confirmed cases'):
        """Get number of new (daily) cases in the whole world"""
        nb_cases = self.get_nb_cases_worldwide(in_terms_of)
        return nb_cases.diff()

    def get_population(self, country):
        """Get the population of the selected country"""
        if country.lower() == 'world':
            return self.wb_data['Population']['World']
        return self.wb_data['Population'][country]

    def get_last_update_date(self):
        """Get the date of the last data recorded"""
        date = self.cases.columns[-1]
        date = pd.to_datetime(date)
        return date


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    data = CovidDataHolder.get_instance()
    print('Top countries: ', data.get_top_countries())
    # cases = data.get_nb_cases('France')
    # deaths = data.get_nb_cases('France', 'deaths')
    cases = data.get_daily_cases('France', 'confirmed cases')
    deaths = data.get_daily_cases('France', 'deaths')
    cases.plot()
    deaths.plot()
    plt.show()
    # print(data.get_population('France'))
    print(data.get_last_update_date())
