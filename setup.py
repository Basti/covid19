# from setuptools import setup, find_packages
from setuptools import setup
from pathlib import Path

readme_file = Path.cwd() / 'Readme.md'
with open(readme_file, 'r') as fid:
    README = fid.read()

with open("requirements.txt") as req:
    install_req = req.read().splitlines()

setup(
    name="QtCovid",
    version='0.1.0',
    description='Qt GUI for plotting COVID-19 data',
    long_description=README,
    long_description_content_type='text/markdown',
    author='Baptiste Véron',
    author_email='baptisteveron@gmail.com',
    licence='MIT',
    install_requires=install_req,
    python_requires='>=3',
    # packages=find_packages(),
    scripts=['covid_qt.py']
    # zip_safe=False,
)
