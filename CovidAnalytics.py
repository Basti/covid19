#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from CovidDataHolder import CovidDataHolder
import pandas as pd
import numpy as np


class CovidAnalytics:
    def __init__(self, country):
        self.country = country.title()
        self.database = CovidDataHolder.get_instance()
        self.data = pd.DataFrame()
        if self.country in self.database.countries:
            self.data['confirmed cases'] = self.database.get_nb_cases(self.country, in_terms_of='confirmed cases')
            self.data['deaths'] = self.database.get_nb_cases(self.country, in_terms_of='deaths')
            self.data['recovered'] = self.database.get_nb_cases(country, in_terms_of='recovered')
            self.data['daily cases'] = self.database.get_daily_cases(self.country, in_terms_of='confirmed cases')
            self.data['daily deaths'] = self.database.get_daily_cases(self.country, in_terms_of='deaths')
        else:
            msg = self.country + ' not found in the database !'
            raise Exception(msg)
        # Correction of negative daily cases
        self.data.loc[self.data['daily cases'] < 0] = np.nan
        self.data.loc[self.data['daily deaths'] < 0] = np.nan
        #
        self.data['days since 1st case'] = [i for i in range(len(self.data))]
        self.population = self.database.get_population(self.country)
        self.base_rate = 100000
        #
        self.compute_prevalence_rate()
        self.compute_incidence_rate('month')
        self.compute_death_rate('month')

    def compute_prevalence_rate(self):
        self.data['prevalence'] = self.base_rate * (self.data['confirmed cases'] - self.data['recovered']) / self.population

    def compute_incidence_rate(self, period):
        nb_days = {'week': 7, 'month': 30, 'day': 1}
        if period in nb_days.keys():
            self.data['incidence'] = self.base_rate * self.data['confirmed cases'].diff(nb_days[period]) / self.population

    def compute_death_rate(self, period):
        nb_days = {'week': 7, 'month': 30, 'day': 1}
        if period in nb_days.keys():
            self.data['death rate'] = self.base_rate * self.data['deaths'].diff(nb_days[period]) / self.population

    def get_last_infos(self):
        d = {}
        for prop in self.data.columns:
            d[prop] = self.data[prop].iloc[-1]
        return d


if __name__ == '__main__':
    import matplotlib.pyplot as plt

    # countries = ['France', 'United States', 'Mexico', 'Germany', 'Spain', 'Italy', 'United Kingdom']
    countries = ['France', 'Germany', 'Spain', 'Italy', 'United Kingdom']
    analytics = [CovidAnalytics(c) for c in countries]
    # selection = ['confirmed cases', 'deaths', 'prevalence']
    selection = ['prevalence', 'death rate', 'incidence']
    for sel in selection:
        plt.figure()
        ax = plt.gca()
        for ca in analytics:
            if sel == 'prevalence':
                ca.compute_prevalence_rate()
            if sel == 'death rate':
                ca.compute_death_rate('month')
            if sel == 'incidence':
                ca.compute_incidence_rate('month')
            ca.data[sel].plot(ax=ax, title=sel, label=ca.country)
        plt.legend(loc='best')
    plt.show()
    # print(analytics[0].get_last_infos())
