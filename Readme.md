# QtCovid

## Description
A simple GUI application for COVID-19 data visualization.

Inspired from
https://github.com/aatishb/covid/blob/master/curvefit.ipynb

## Data source
Data pulled from the Johns Hopkins Data Repository of global Coronavirus COVID-19 cases
https://github.com/CSSEGISandData/COVID-19

Population from WHO data :
https://www.who.int/data/gho/data/indicators/indicator-details/GHO/population-(in-thousands)
